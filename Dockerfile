FROM debian:buster
LABEL maintainer="Roudier Lucile"

RUN apt-get update && apt-get install -y procps && apt-get install nano && apt-get install -y wget
RUN apt-get -y install php7.3-fpm php7.3-common php7.3-mysql php7.3-gmp php7.3-curl php7.3-intl php7.3-mbstring php7.3-xmlrpc php7.3-gd php7.3-xml php7.3-cli php7.3-zip php7.3-soap php7.3-imap
RUN apt-get -y install wget
RUN apt-get -y install nginx
RUN apt-get -y install mariadb-server
RUN apt-get -y install openssl
RUN apt-get -y install gettext-base

ENV auto_index=off

COPY ./src/init_server.sh ./
COPY ./src/nginx-conf ./tmp/nginx-conf
COPY ./src/config.inc.php ./tmp/config.inc.php
COPY ./src/wp-config.php ./tmp/wp-config.php

CMD bash init_server.sh
