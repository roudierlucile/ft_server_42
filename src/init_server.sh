#!/bin/sh

# we config access 'cause by default we don't have the right
chown -R www-data /var/www/*
chmod -R 755 /var/www/*

# we create the website in www 'cause every site are int "www" folder
mkdir /var/www/localhost && touch /var/www/localhost/index.php

#config NGINX
#we move conf file in sites-availabme
#we create a link
# Env Variable
envsubst '${auto_index}' < ./tmp/nginx-conf > ./tmp/localhost
mv ./tmp/localhost /etc/nginx/sites-available/localhost
ln -s /etc/nginx/sites-available/localhost /etc/nginx/sites-enabled/localhost

# Config MYSQL
#We create a Database who called wordpress 'cause we used wordpress'
#we skip password 'caue we don't care.
#we rise the privileges it's a db thing 

service mysql start
echo "CREATE DATABASE wordpress;" | mysql -u root 
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress_admin'@'localhost' IDENTIFIED BY 'password';" | mysql -u root
echo "FLUSH PRIVILEGES;" | mysql -u root

# phpmyadmin
# we create a folder in localhost
# we download the file and we move into the good folder
mkdir /var/www/localhost/phpmyadmin
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz
tar -xvf phpMyAdmin-4.9.0.1-all-languages.tar.gz --strip-components 1 -C /var/www/localhost/phpmyadmin
mv ./tmp/phpmyadmin.inc.php /var/www/localhost/phpmyadmin/config.inc.php

#install wordpress
# we install the wordpress into tmp
# we move into our website

cd /tmp/
wget -c https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
mv wordpress/ /var/www/localhost
mv /tmp/wp-config.php /var/www/localhost/wordpress

#install SSl

mkdir /etc/nginx/ssl
openssl req -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/nginx/ssl/localhost.pem -keyout /etc/nginx/ssl/localhost.key -subj "/C=FR/ST=Paris/L=Paris/O=42 School/OU=lroudier/CN=localhost"



#We start the services
service nginx start
service mysql restart
/etc/init.d/php7.3-fpm start
/etc/init.d/php7.3-fpm status
sleep infinity